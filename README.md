robofirm/wkhtmltopdf Puppet Module
==========================

#### Table of Contents

1. [Module Description](#module-description)
2. [Setup - The basics of getting started with robofirm/wkhtmltopdf](#setup)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Module description

This module installs wkhtmltopdf, versions 0.12.2 or higher on CentOS/RedHat 7. 

## Setup

Add this to your profile:
```
include wkhtmltopdf
```

## Usage

All configuration is done via Hiera. Here is an example common configuration:
```yaml
wkhtmltopdf::ensure: present
wkhtmltopdf::version: 0.12.4
```

## Reference


## Limitations

OS Support is currently limited to CentOS or RedHat. Only versions 0.12.2 and higher are
supported as older versions required an X server to be running and are not worth trying to support. 

## Development

Open source is new to us at Robofirm, so this is TBD. Please raise issues in our 
[Bitbucket issue tracker](https://bitbucket.org/rmgmedia/puppet-module-wkhtmltopdf/issues).