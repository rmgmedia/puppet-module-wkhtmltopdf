Robofirm Wkhtmltopdf Puppet Module Changelog
============================================

# 0.1.2 (Unreleased)
* Fixed removal of symlinks when ensure is absent

# 0.1.1
* Coding standards fix

# 0.1.0
* Initial commit; Supports CentOS/RedHat 7 only
